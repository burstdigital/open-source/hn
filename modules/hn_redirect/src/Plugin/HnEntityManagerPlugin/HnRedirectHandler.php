<?php

namespace Drupal\hn_redirect\Plugin\HnEntityManagerPlugin;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;
use Drupal\hn\Plugin\HnEntityManagerPluginBase;
use Symfony\Component\HttpFoundation\Request;

/**
 * This RedirectHandler formats a Redirect uri so it's usable in front-ends.
 *
 * @HnEntityManagerPlugin(
 *   id = "hn_redirect",
 *   priority = 10,
 * )
 */
class HnRedirectHandler extends HnEntityManagerPluginBase {

  protected $supports = '\Drupal\redirect\Entity\Redirect';

  /**
   * {@inheritdoc}
   */
  public function handle(EntityInterface $entity, $view_mode = 'default') {

    $normalized_entity = [
      '__hn' => [
        'view_modes' => 'default',
      ],
    ] + \Drupal::getContainer()->get('serializer')->normalize($entity);

    // Get uri from redirect destination.
    $uri = $entity->getTypedData()->get('redirect_redirect')->getValue()[0]['uri'];
    /** @var \Drupal\Core\Url $url */
    $url = Url::fromUri($uri);

    // Check whether to retain query parameters.
    $passthrough_query_string = \Drupal::config('redirect.settings')->get('passthrough_querystring');
    if ($passthrough_query_string) {
      // Create new Request from original path to split pathname & query.
      /** @var \Symfony\Component\HttpFoundation\Request $request */
      $request = Request::create(\Drupal::request()->get('path'));
      // Get original query parameters and overwrite the params from the
      // redirect entity.
      $url->setOption('query', (array) $url->getOption('query') + $request->query->all());
    }

    // Overwrite the redirect uri with the url.
    $normalized_entity['redirect_redirect']['uri'] = $url->toString();

    return $normalized_entity;

  }

}
