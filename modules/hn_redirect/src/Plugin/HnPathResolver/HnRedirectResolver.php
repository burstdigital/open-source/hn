<?php

namespace Drupal\hn_redirect\Plugin\HnPathResolver;

use Drupal\hn\HnPathResolverResponse;
use Drupal\hn\Plugin\HnPathResolverBase;
use Symfony\Component\HttpFoundation\Request;

/**
 * This PathResolver returns a Redirect entity if available for a path.
 *
 * @HnPathResolver(
 *   id = "hn_redirect_redirect_resolver",
 *   priority = 10
 * )
 */
class HnRedirectResolver extends HnPathResolverBase {

  /**
   * {@inheritdoc}
   */
  public function resolve($path) {
    $redirect_service = \Drupal::service('redirect.repository');

    // Create new Request from path to split pathname & query.
    /** @var \Symfony\Component\HttpFoundation\Request $request */
    $request = Request::create($path);

    $request->getPreferredLanguage();
    // Get pathname from request path.
    /** @var \Drupal\redirect\RedirectRepository $redirect_service */
    $source_path = $request->getPathInfo();
    // Source path has no leading /.
    $source_path = trim($source_path, '/');

    // Get query from request path.
    $query = $request->query->all();
    /** @var \Drupal\redirect\Entity\Redirect $redirect */

    // Get all redirects by original request path.
    $redirect = $redirect_service->findMatchingRedirect($source_path, $query, 'en');

    if (empty($redirect)) {
      return NULL;
    }

    // Get status (301/302).
    $status = (int) $redirect->getStatusCode();

    return new HnPathResolverResponse($redirect, $status);
  }

}
