<?php

namespace Drupal\hn_content_translation\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Headless Ninja Entity Translation Plugin item annotation object.
 *
 * @see \Drupal\hn\Plugin\HnEntityTranslationPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class HnEntityTranslationPlugin extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

}
