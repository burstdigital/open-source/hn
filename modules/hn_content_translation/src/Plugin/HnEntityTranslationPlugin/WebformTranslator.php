<?php

namespace Drupal\hn_content_translation\Plugin\HnEntityTranslationPlugin;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\hn_content_translation\Plugin\HnEntityTranslationPluginBase;

/**
 * Provides a HN Entity Translator for the Webform Entity.
 *
 * @HnEntityTranslationPlugin(
 *   id = "hn_webform"
 * )
 */
class WebformTranslator extends HnEntityTranslationPluginBase {

  protected $supports = '\Drupal\webform\Entity\Webform';

  /**
   * @inheritdoc
   */
  public function isSupported(EntityInterface $entity) {
    return $entity instanceof $this->supports && $entity->hasTranslations();
  }

  /**
   * @inheritdoc
   */
  public function translate(EntityInterface &$entity, $view_mode = 'default') {

    $requested_langcode = \Drupal::languageManager()
      ->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)
      ->getId();

    /** @var \Drupal\webform\WebformTranslationManagerInterface $translation_manager */
    $translation_manager = \Drupal::service('webform.translation_manager');

    $translated_elements = $translation_manager->getElements($entity, $requested_langcode);
    $elements = array_replace_recursive($entity->getElementsDecoded(), $translated_elements);

    $entity->setElements($elements);
    $entity->set('uuid', $entity->uuid() . '--' . $requested_langcode);
  }

}
