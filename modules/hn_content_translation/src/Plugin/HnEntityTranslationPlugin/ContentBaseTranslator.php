<?php

namespace Drupal\hn_content_translation\Plugin\HnEntityTranslationPlugin;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\hn_content_translation\Plugin\HnEntityTranslationPluginBase;

/**
 * Provides a HN Entity Translator for Entity that are instance of ContentBase.
 *
 * @HnEntityTranslationPlugin(
 *   id = "hn_content_base"
 * )
 */
class ContentBaseTranslator extends HnEntityTranslationPluginBase {

  protected $supports = 'Drupal\Core\Entity\ContentEntityBase';

  /**
   * @inheritdoc
   */
  public function isSupported(EntityInterface $entity) {
    return
      $entity instanceof $this->supports &&
      $entity->getEntityType()->hasKey('uuid') &&
      $entity->isTranslatable();
  }

  /**
   * {@inheritdoc}
   */
  public function translate(EntityInterface &$entity, $view_mode = 'default') {

    $requested_langcode = \Drupal::languageManager()
      ->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)
      ->getId();

    // Default language for the Entity.
    $langcode = $entity->language()->getId();

    // Entity UUID.
    $uuid_key = $entity->getEntityType()->getKey('uuid');

    if ($entity->hasTranslation($requested_langcode)) {

      $translated_entity = $entity->getTranslation($requested_langcode);

      // Override the default langcode with the requested one as the entity
      // has a translation for this language.
      $langcode = $requested_langcode;

      $translatable_fields = $translated_entity->getTranslatableFields();

      unset($translatable_fields['langcode']);

      foreach ($translatable_fields as $field_name => $field) {
        try {
          // Replace current field with the translated field.
          $translated_value = $translated_entity->get($field_name)->getValue();

          $entity->set($field_name, $translated_value);
        }
        catch (\Exception $error) {

        }
      }
    }
    $entity->set($uuid_key, $entity->uuid() . '--' . $langcode);
  }

}
