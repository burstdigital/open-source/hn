<?php

namespace Drupal\hn_content_translation\Plugin\HnEntityTranslationPlugin;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\hn_content_translation\Plugin\HnEntityTranslationPluginBase;

/**
 * Provides a HN Entity Translator for the Webform Entity.
 *
 * @HnEntityTranslationPlugin(
 *   id = "hn_view"
 * )
 */
class ViewTranslator extends HnEntityTranslationPluginBase {

  protected $supports = '\Drupal\views\Entity\View';

  /**
   * {@inheritdoc}
   */
  public function translate(EntityInterface &$entity, $view_mode = 'default') {

    $requested_langcode = \Drupal::languageManager()
      ->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)
      ->getId();

    // @todo: if Config translation is enabled?.
    $translations = \Drupal::languageManager()->getLanguageConfigOverride($requested_langcode, $entity->getConfigDependencyName());
    $view_entity_translations = $translations->get();

    // Override the display with the config translation.
    if (isset($view_entity_translations['display'])) {
      $entity->set('display', array_replace_recursive($entity->get('display'), $view_entity_translations['display']));
      $entity->set('uuid', $entity->uuid() . '--' . $requested_langcode);
    }

  }

}
