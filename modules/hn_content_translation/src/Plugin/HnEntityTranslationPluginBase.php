<?php

namespace Drupal\hn_content_translation\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Entity\EntityInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for Headless Ninja Entity Manager Plugin plugins.
 */
abstract class HnEntityTranslationPluginBase extends PluginBase implements HnEntityTranslationPluginInterface {

  /**
   * The interface or class that this HnEntityManager supports.
   *
   * @var string|\stdClass
   */
  protected $supports;

  /**
   * LanguageManager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * HnEntityTranslationPluginBase constructor.
   *
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *
   * @internal param \Drupal\hn_content_translation\Plugin\EntityManagerInterface $entity_manager
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->languageManager = \Drupal::languageManager();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function isSupported(EntityInterface $entity) {
    return $entity instanceof $this->supports;
  }

}
