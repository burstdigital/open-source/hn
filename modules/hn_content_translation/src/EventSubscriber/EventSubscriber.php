<?php

namespace Drupal\hn_content_translation\EventSubscriber;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Url;
use Drupal\hn\Event\HnHandledEntityEvent;
use Drupal\hn\Event\HnResponseEvent;
use Drupal\node\Entity\Node;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class DefaultSubscriber.
 */
class EventSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      HnResponseEvent::CREATED_CACHE_MISS => 'alterResponseData',
      HnHandledEntityEvent::POST_HANDLE => 'postHandle',
    ];
  }

  /**
   * Alters the response data.
   *
   * @param \Drupal\hn\Event\HnResponseEvent $event
   *   The event that was dispatched.
   */
  public function alterResponseData(HnResponseEvent $event) {
    $responseData = $event->getResponseData();

    /** @var \Drupal\Core\Language\LanguageManagerInterface $language_manager */
    $language_manager = \Drupal::service('language_manager');
    $lang_id = $language_manager->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();

    if (isset($responseData['data']['config__menus'])) {
      $responseData['data']['config__menus--' . $lang_id] = $responseData['data']['config__menus'];
      unset($responseData['data']['config__menus']);
    }

    if (isset($responseData['data']['config__entities'])) {
      foreach ($responseData['data']['config__entities'] as $name => $values) {
        $translation = $language_manager->getLanguageConfigOverride($lang_id, $name)->get();
        $translated_config = array_replace_recursive($values, $translation);
        $responseData['data']['config__entities--' . $lang_id][$name] = $translated_config;
      }
      unset($responseData['data']['config__entities']);
    }

    $event->setResponseData($responseData);
  }

  /**
   * Alters the response data.
   *
   * @param \Drupal\hn\Event\HnHandledEntityEvent $event
   *   The event that was dispatched.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function postHandle(HnHandledEntityEvent $event) {
    $entity = $event->getEntity();
    $normalized_entity = $event->getHandledEntity();

    $activeLangcode = \Drupal::languageManager()
      ->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)
      ->getId();

    if (!$entity instanceof ContentEntityInterface) {
      return;
    }

    $normalized_entity['langcode'] = $entity->hasTranslation($activeLangcode) ? $activeLangcode : 'und';

    if ($entity instanceof Node && $entity->hasLinkTemplate('canonical')) {
      $normalized_entity['__hn']['locale_paths'] = [];

      // Get all available languages.
      $languages = \Drupal::languageManager()->getLanguages();

      foreach ($languages as $language) {
        if ($entity->getTranslationStatus($language->getId())) {
          // Drupal has a hard time working with the $entity object and its translations, so we format the url from scratch using the node id.
          $canonical = '/' . trim(Url::fromRoute('entity.node.canonical', ['node' => $entity->id()], ['language' => $language])->toString(), '/');

          // Remove the frontpage url if it's present in the canonical.
          // TODO: Get the correct frontpage url directly from Drupal, this method feels a little hacky.
          $frontpage_url = \Drupal::config('system.site')->get('page.front');
          $canonical = '/' . trim(str_replace($frontpage_url, '', $canonical), '/');

          $normalized_entity['__hn']['locale_paths'][$language->getId()] = \Drupal::service('path_alias.manager')->getAliasByPath($canonical);
        }
      }

      $normalized_entity['__hn']['url'] = Url::fromRoute('entity.node.canonical', ['node' => $entity->id()], ['langcode' => $activeLangcode])->toString();

    }

    $event->setHandledEntity($normalized_entity);
  }

}
