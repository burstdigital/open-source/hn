<?php

namespace Drupal\hn_cache_session\Form;

use Drupal\Core\Database\Database;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ConfigForm.
 */
class SettingsForm extends ConfigFormBase
{

  static $BATCH_LIMIT = 50;

  /**
   * Set-up batch and start it
   * @param array $form
   * @param FormStateInterface $form_state
   */
  public static function clearExpiredRequests(array &$form, FormStateInterface $form_state)
  {
    $batch = [
      'title' => t('Clear expired requests'),
      'operations' => [],
      'finished' => [self::class, 'batchFinished'],
      'init_message' => t('Starting deletion of expired requests...'),
      'progress_message' => t('Completed @current/@total batches.'),
      'error_message' => t('Something went wrong.'),
    ];


    // Add operations for both verified and unverified requests
    self::addOperations($batch['operations'], 'verified');
    self::addOperations($batch['operations'], 'unverified');

    // Start the batch
    batch_set($batch);
  }

  /**
   * Add operations for provided type
   * @param $operations Referenced array with operations to extend
   * @param $type Type of request cache (e.g. verified)
   */
  private static function addOperations(&$operations, $type)
  {
    $connection = Database::getConnection();

    // Get timestamp that will be used to check expiration timestamps against
    $expireTimestamp = self::getExpireTimestamp($type);

    // If timestamp isn't set by user, skip this type
    if (!$expireTimestamp) return;

    // Get a count of all requests that should expire
    $result = $connection->query('SELECT count(name) as total from key_value_expire WHERE name = :name AND expire > :expire', [
      ':name' => 'requests.' . $type,
      ':expire' => $expireTimestamp,
      ])->fetchAssoc();

    $total = $result['total'] ?: 0;
    if ($total > 0) {
      // Get numberOfBatches needed to clear all expired requests
      $numberOfBatches = ceil($total / self::$BATCH_LIMIT);
      for ($offset = 0; $offset < $numberOfBatches; $offset++) {
        // Add operation to run runBathOperation method for each badge
        $operations[] = [[self::class, 'runBatchOperation'], [$type, $total, $expireTimestamp]];
      }
    }
  }

  /**
   * This method is called for every operation and will delete self::$BADGE_LIMIT number of requests per operation
   * @param $type Type of request cache (e.g. verified)
   * @param $total Number of expired requests to delete
   * @param $expireTimestamp All requests expiring after this timestamp will be deleted
   * @param $context Referenced object shared between operations
   */
  public static function runBatchOperation($type, $total, $expireTimestamp, &$context)
  {
    $connection = Database::getConnection();

    // Delete all requests that should expire
    $connection->query('DELETE FROM key_value_expire WHERE name = :name AND expire > :expire LIMIT 50', [
      ':name' => 'requests.' . $type,
      ':expire' => $expireTimestamp,
    ]);

    // Set total to context to use in batchFinished method
    $context['results']['total'][$type] = $total;
  }

  /**
   * Called when all operations are finished
   * @param $success
   * @param array $results
   * @param array $operations
   */
  public static function batchFinished($success, array $results, array $operations)
  {
    if ($success) {
      // Get total number of requests deleted
      $total = 0;
      foreach ($results['total'] as $type) {
        $total += $type;
      }
      drupal_set_message(t('Finished with deleting :total expired requests', [':total' => $total]), 'status');
    } else {
      drupal_set_message(t('Something went wrong'), 'status');
    }
  }

  /**
   * Get timestamp at which requests should expire given the current config for the provided type
   * @param $type Type of request cache (e.g. verified)
   * @return bool|int Returns timestamp or FALSE if no config is set for this type
   */
  private static function getExpireTimestamp($type)
  {
    $config = \Drupal::config('hn_cache_session.settings');
    $setting = $config->get('expire_' . $type);
    if ($setting === NULL || $setting === '') {
      drupal_set_message(t('Please enter a value for the <strong>\'Expire value for :name requests\'</strong> field first. Skipping these type of requests for now.', [':name' => $type]), 'warning');
      return FALSE;
    }
    return time() + $setting;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames()
  {
    return [
      'hn_cache_session.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'hn_cache_session_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $config = $this->config('hn_cache_session.settings');

    $form['expire_verified'] = [
      '#type' => 'number',
      '#title' => t('Expire value for verified requests'),
      '#description' => t('Enter the expire value in seconds after which verified requests should be deleted by the cron job. After this expire time is passed, the user will receive the data again. Defaults to 86400 (1 day)'),
      '#default_value' => $config->get('expire_verified'),
      '#min' => 0,
    ];

    $form['expire_unverified'] = [
      '#type' => 'number',
      '#title' => t('Expire value for unverified requests'),
      '#description' => t('Enter the expire value in seconds after which unverified requests should be deleted by the cron job. After this expire time is passed, the user will receive the data again. Defaults to 1800 (30 minutes)'),
      '#default_value' => $config->get('expire_unverified'),
      '#min' => 0,
    ];

    $form['clear_expired_requests'] = [
      '#type' => 'submit',
      '#value' => t('Clear expired requests'),
      '#description' => t('When this button is clicked, all requests that are older than the above thresholds are deleted'),
      '#submit' => ['::clearExpiredRequests'],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $values = $form_state->getValues();

    // Save the config.
    $this->config('hn_cache_session.settings')
      ->set('expire_verified', $values['expire_verified'])
      ->set('expire_unverified', $values['expire_unverified'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
