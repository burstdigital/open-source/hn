<?php

namespace Drupal\hn_cache_session;

use Drupal\user\SharedTempStore;
use Drupal\user\SharedTempStoreFactory;

class HnCacheSessionTempStoreFactory extends SharedTempStoreFactory
{
  /**
   * Creates a HnCacheSessionTempStore for the current user or anonymous session.
   *
   * @param string $user
   *   The user id to use for this key/value store.
   * @param number $expire
   *
   *
   * @return \Drupal\user\SharedTempStore
   *   An instance of the key/value store.
   */
  public function getHnCacheSession($user, $expire)
  {
    // Store the data for this collection in the database.
    $storage = $this->storageFactory->get("user.shared_tempstore.hn_cache_session.$user");
    return new SharedTempStore($storage, $this->lockBackend, $user, $this->requestStack, $expire);
  }
}
