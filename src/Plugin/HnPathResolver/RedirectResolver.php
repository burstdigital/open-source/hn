<?php

namespace Drupal\hn\Plugin\HnPathResolver;

use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Url;
use Drupal\hn\HnPathResolverResponse;
use Drupal\hn\Plugin\HnPathResolverBase;

/**
 * This provides a 404 resolver.
 *
 * @HnPathResolver(
 *   id = "hn_redirect",
 *   priority = -10
 * )
 */
class RedirectResolver extends HnPathResolverBase {

  /**
   * {@inheritdoc}
   */
  public function resolve($path) {
    $redirect_service = \Drupal::service('redirect.repository');

    // Source path has no leading /.
    $source_path = trim($path, '/');

    // @todo remove this in certain time.
    if (preg_match('@index\.php/(nl|en|de|fr|pl|es)(/[c|k]onta[c|k]t)@', $source_path) !== 0) {
      $source_path = 'index/contact';
    }
    elseif (preg_match('@index\.php/(nl|en|de|fr|pl|es)(/.*)@', $source_path) !== 0) {
      $source_path = preg_replace('@index\.php/(nl|en|de|fr|pl|es)(/.*)@', '$2', $path);
    }

    $source_path = trim($source_path, '/');

    /** @var \Drupal\redirect\Entity\Redirect $redirect */
    // Get all redirects by original url.
    $langcode = \Drupal::languageManager()->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();
    $redirect = $redirect_service->findMatchingRedirect($source_path, [], $langcode);
    if (empty($redirect)) {
      return NULL;
    }
    // Get 301/302.
    $status = (int) $redirect->getStatusCode();
    // Get the redirect uri.
    $uri = $redirect->getRedirect()['uri'];
    // Check if it is an internal url.
    $url = Url::fromUri($uri);
    if ($url->isExternal()) {
      return new HnPathResolverResponse($redirect, $status);
    }

    try {
      $internal_path = $url->getInternalPath();
    }
    catch(\UnexpectedValueException $exception){
      return NULL;
    }

    /** @var \Drupal\hn\Plugin\HnPathResolverManager $path_resolver */
    $path_resolver = \Drupal::service('hn.path_resolver');
    $entity = $path_resolver->resolve($internal_path)->getEntity();

    return new HnPathResolverResponse($entity, $status);
  }
}
