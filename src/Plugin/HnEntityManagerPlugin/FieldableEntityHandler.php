<?php

namespace Drupal\hn\Plugin\HnEntityManagerPlugin;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\hn\Plugin\HnEntityManagerPluginBase;

/**
 * Provides a HN Entity Handler for the FieldableEntity entity.
 *
 * @HnEntityManagerPlugin(
 *   id = "hn_fieldable_entity"
 * )
 */
class FieldableEntityHandler extends HnEntityManagerPluginBase {

  protected $supports = 'Drupal\Core\Entity\FieldableEntityInterface';

  /**
   * {@inheritdoc}
   */
  public function handle(EntityInterface $entity, $view_mode = 'default') {
    /** @var \Drupal\Core\Entity\FieldableEntityInterface $entity */
    /** @var \Drupal\hn\HnResponseService $responseService */
    $responseService = \Drupal::getContainer()->get('hn.response');
    $serializer = \Drupal::getContainer()->get('serializer');

    // If the current user doesn't have permission to view, don't add.
    if (!$entity->access('view', \Drupal::getContainer()->get('current_user'))) {
      $responseService->log('Not adding entity ' . $entity->uuid() . ', no permission.');
      return [];
    }

    $responseService->log('[' . $entity->uuid() . '] Adding entity.');

    $entity_with_views = $responseService->entitiesWithViews->addEntity($entity, $view_mode);
    if (!$entity_with_views) {
      $responseService->log('[' . $entity->uuid() . '] View already added, not adding.');
      return [];
    }

    $responseService->log('[' . $entity->uuid() . '] Getting visible fields..');

    $visible_fields = $entity_with_views->getVisibleFields();
    // Get key fields (i.e. fields that are fundamental to this entity type).
    $entity_definition = \Drupal::entityTypeManager()->getDefinition($entity->getEntityTypeId());
    $entity_keys = $entity_definition->getKeys();

    $visible_fields = array_merge(array_values($entity_keys), $visible_fields);

    $visible_fields = array_unique($visible_fields);

    $responseService->log('[' . $entity->uuid() . '] Normalizing..');

    // Todo, find a better way to get the final list of visible fields.
    $hide_fields = \Drupal::config('hn_cleaner.settings')->get('fields.' . $entity->getEntityTypeId());
    if (!empty($hide_fields)) {
      $all_type_fields = array_keys(\Drupal::service('entity_field.manager')->getBaseFieldDefinitions($entity->getEntityTypeId()));
      $diff_fields = array_values(array_diff($all_type_fields, $hide_fields));
      $visible_fields = array_merge($diff_fields, $visible_fields);
    }

    // Do not normalize metatags if they're not in the display.
    if (
      ($key = array_search('metatag', $visible_fields)) !== FALSE &&
      (array_search('field_metatags', $visible_fields) === FALSE
      && array_search('field_meta_tags', $visible_fields) === FALSE)
    ) {
      unset($visible_fields[$key]);
    }

    $normalized_fields = [];
    foreach ($visible_fields as $visible_field) {
      if ($entity->hasField($visible_field)) {
        $field = $entity->get($visible_field);
        $normalized_fields[$visible_field] = $serializer->normalize($field);

        if ($field instanceof EntityReferenceFieldItemListInterface) {

          // Get all referenced entities.
          $referenced_entities = $field->referencedEntities();

          // Get the referenced view mode (e.g. teaser) that is set in the current
          // display (e.g. full).
          $referenced_entities_display = $entity_with_views->getDisplay($view_mode)->getComponent($visible_field);
          $referenced_entities_view_mode = $referenced_entities_display && $referenced_entities_display['type'] === 'entity_reference_entity_view' ? $referenced_entities_display['settings']['view_mode'] : 'default';
          //
          foreach ($referenced_entities as $referenced_entity) {
            $responseService->addEntity($referenced_entity, $referenced_entities_view_mode);
          }
        }

      }
    }

    return [
        '__hn' => [
          'view_modes' => $entity_with_views->getViewModes(),
        ],
      ] + $normalized_fields;
  }

}
